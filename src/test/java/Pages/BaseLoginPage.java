package Pages;

import Helpers.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public abstract class BaseLoginPage extends BasePage
{
    protected By EmailEntry = By.name("username");
    protected By PasswordEntry = By.name("password");
    protected By SubmitBtn = By.xpath("//input[@type='submit']");

    public BaseLoginPage(WebDriver driver) {
        super(driver);
    }

    @Step
    public BaseLoginPage EnterEmail(String text)
    {
        Enter(EmailEntry, text);
        TakeScreenshot(driver, "EnterEmail");

        return this;
    }

    @Step
    public BaseLoginPage EnterPassword(String text)
    {
        Enter(PasswordEntry, text);
        TakeScreenshot(driver, "EnterPassword");

        return this;
    }

    @Step
    public BaseLoginPage TapSubmit()
    {
        Tap(SubmitBtn);
        TakeScreenshot(driver, "TapSubmit");

        return this;
    }
}
