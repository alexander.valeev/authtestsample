package Pages;

import Helpers.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class BaseProfilePage extends BasePage
{
    private By UsernameLabel = By.id("username");

    public BaseProfilePage(WebDriver driver) {
        super(driver);
    }

    public BaseProfilePage InitDone(String username)
    {
        assertTrue(Wait(UsernameLabel).getText().equals(username));

        return this;
    }
}
