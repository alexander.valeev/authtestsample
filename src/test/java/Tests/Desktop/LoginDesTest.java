package Tests.Desktop;

import Helpers.BaseTestFixture;
import Pages.Desktop.LoginDesPage;
import Pages.Desktop.ProfileDesPage;
import org.junit.jupiter.api.Test;

public class LoginDesTest extends BaseTestFixture
{
    @Test
    public void Login()
    {
        testName = new Object(){}.getClass().getEnclosingMethod().getName();

        String username = "test";

        new LoginDesPage(driver)
                .EnterEmail(username)
                .EnterPassword("test")
                .TapSubmit();

        new ProfileDesPage(driver)
                .InitDone(username);
    }
}
